export class Task {
  constructor(
    public id: string,
    public title: string,
    public user: string,
    public status: string
  ) {
  }
}

export interface TaskData {
  title: string,
  user: string,
  status: string
}

export interface TaskChange {
  user: string,
  status: string,
  id: string
}

export interface ApiTaskData {
  _id: string,
  title: string,
  user: string,
  status: string
}
