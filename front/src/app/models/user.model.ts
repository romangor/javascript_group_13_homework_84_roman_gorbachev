export class User {
  constructor(
    public id: string,
    public name: string
  ) {
  }
}

export interface UserData {
  name: string;
}

export interface ApiUserData {
  _id: string,
  name: string,
}
