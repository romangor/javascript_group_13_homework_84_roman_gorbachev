import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiTaskData, Task, TaskChange, TaskData } from '../models/task.model';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { ApiUserData, User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  constructor(private http: HttpClient) {
  }

  getTasks() {
    return this.http.get<ApiTaskData[]>(environment.url + '/tasks').pipe(
      map(response => {
        return response.map(taskData => {
          return new Task(
            taskData._id,
            taskData.title,
            taskData.user,
            taskData.status
          );
        });
      }))
  };

  createTask(taskData: TaskData) {
    if (taskData.user === '') {
      taskData.user = 'N/A';
    }
    const task = {
      user: taskData.user,
      title: taskData.title,
    }
    return this.http.post(environment.url + '/tasks', task);
  };

  editTask(task: TaskChange) {
    const body = {
      status: task.status,
      user: task.user,
    }
    return this.http.put(environment.url + `/tasks/${task.id}`, body);
  };

  removeTask(id: string) {
    return this.http.delete(environment.url + `/tasks/${id}`);
  };

  getUsers() {
    return this.http.get<ApiUserData[]>(environment.url + `/users`).pipe(
      map(response => {
        return response.map(userData => {
          return new User(
            userData._id, userData.name
          )
        })
      })
    )
  }
}
