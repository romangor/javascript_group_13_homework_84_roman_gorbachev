import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  createTaskError,
  createTaskRequest,
  createTaskSuccess, editTaskError, editTaskRequest, editTaskSuccess,
  fetchTasksError,
  fetchTasksRequest,
  fetchTasksSuccess, removeTaskError, removeTaskRequest, removeTaskSuccess
} from './tasks.actions';
import { catchError, mergeMap, of, tap } from 'rxjs';
import { AppService } from '../services/app.service';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';


@Injectable()
export class TasksEffects {
  constructor(
    private actions: Actions,
    private appService: AppService,
    private router: Router
  ) {
  }

  fetchTasks = createEffect(() => this.actions.pipe(
    ofType(fetchTasksRequest),
    mergeMap(() => this.appService.getTasks().pipe(
      map(tasks => fetchTasksSuccess({tasks})),
      catchError(() => of(fetchTasksError({
        error: 'Error'
      })))
    ))
  ));

  createTask = createEffect(() => this.actions.pipe(
    ofType(createTaskRequest),
    mergeMap(({taskData}) => this.appService.createTask(taskData).pipe(
      map(() => createTaskSuccess()),
      tap(() => this.router.navigate(['/'])),
      catchError(() => of(createTaskError({error: 'Incorrect data'})))
    ))
  ));

  editTask = createEffect(() => this.actions.pipe(
    ofType(editTaskRequest),
    mergeMap(({taskData}) => this.appService.editTask(taskData).pipe(
      map(() => editTaskSuccess()),
      catchError(() => of(editTaskError({error: 'Incorrect data'})))
    ))
  ));

  removeTask = createEffect(() => this.actions.pipe(
    ofType(removeTaskRequest),
    mergeMap(({taskId}) => this.appService.removeTask(taskId).pipe(
      map(() => removeTaskSuccess()),
      catchError(() => of(removeTaskError({error: 'Error'})))
    ))
  ));
}
