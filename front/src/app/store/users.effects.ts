import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, mergeMap, of } from 'rxjs';
import { fetchUsersError, fetchUsersRequest, fetchUsersSuccess } from './users.actions';
import { map } from 'rxjs/operators';
import { AppService } from '../services/app.service';

@Injectable()
export class UsersEffects {
  fetchUsers = createEffect(() => this.actions.pipe(
    ofType(fetchUsersRequest),
    mergeMap(() => this.appService.getUsers().pipe(
      map(users => fetchUsersSuccess({users})),
      catchError(() => of(fetchUsersError({error: 'Error'})))
    ))
  ));

  constructor(
    private actions: Actions,
    private appService: AppService,
  ) {
  }
}
