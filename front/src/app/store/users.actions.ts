import { createAction, props } from '@ngrx/store';
import { User } from '../models/user.model';


export const fetchUsersRequest = createAction('[Users] Fetch Request');
export const fetchUsersSuccess = createAction('[Users] Fetch Success', props<{ users: User[] }>());
export const fetchUsersError = createAction('[Users] Fetch Error', props<{ error: string }>());
