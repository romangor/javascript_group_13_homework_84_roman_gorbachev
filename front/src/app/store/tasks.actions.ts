import { createAction, props } from '@ngrx/store';
import { TaskChange, Task, TaskData } from '../models/task.model';


export const fetchTasksRequest = createAction('[Tasks] Fetch Request');
export const fetchTasksSuccess = createAction('[Tasks] Fetch Success', props<{ tasks: Task[] }>());
export const fetchTasksError = createAction('[Tasks] Fetch Error', props<{ error: string }>());
export const createTaskRequest = createAction('[Task] Create Request', props<{ taskData: TaskData }>());
export const createTaskSuccess = createAction('[Task] Create Success');
export const createTaskError = createAction('[Task] Create Error', props<{ error: string }>());
export const editTaskRequest = createAction('[Task] Edit Request', props<{ taskData: TaskChange }>());
export const editTaskSuccess = createAction('[Task] Edit Success');
export const editTaskError = createAction('[Task] Edit Error', props<{ error: string }>());
export const removeTaskRequest = createAction('[Task] Remove Request', props<{ taskId: string }>());
export const removeTaskSuccess = createAction('[Task] Remove Success');
export const removeTaskError = createAction('[Task] Remove Error', props<{ error: string }>());

