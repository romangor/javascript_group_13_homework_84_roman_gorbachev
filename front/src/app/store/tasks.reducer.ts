import { createReducer, on } from '@ngrx/store';
import { TaskState } from './types'
import {
  createTaskError,
  createTaskRequest,
  createTaskSuccess, editTaskError, editTaskRequest, editTaskSuccess,
  fetchTasksError,
  fetchTasksRequest,
  fetchTasksSuccess, removeTaskError, removeTaskRequest, removeTaskSuccess
} from './tasks.actions';

const initialState: TaskState = {
  tasks: [],
  fetchLoading: false,
  fetchError: null,
  createLoading: false,
  createError: null,
  changeLoading: false,
  changeError: null,
  removeLoading: false,
  removeError: null
}

export const tasksReducer = createReducer(
  initialState,
  on(fetchTasksRequest, state => ({
    ...state,
    fetchLoading: true
  })),
  on(fetchTasksSuccess, (state, {tasks}) => ({
    ...state,
    fetchLoading: false,
    tasks
  })),
  on(fetchTasksError, (state, {error}) => ({
    ...state,
    fetchLoading: false,
    fetchError: error
  })),
  on(createTaskRequest, (state, {taskData}) => ({
    ...state,
    createLoading: true,
    taskData
  })),
  on(createTaskSuccess, state => ({
    ...state,
    createLoading: false,
  })),
  on(createTaskError, (state, {error}) => ({
    ...state,
    createLoading: false,
    createError: error
  })),
  on(editTaskRequest, (state, {taskData}) => ({
    ...state,
    changeLoading: true,
    taskData
  })),
  on(editTaskSuccess, state => ({
    ...state,
    changeLoading: false,
  })),
  on(editTaskError, (state, {error}) => ({
    ...state,
    changeLoading: false,
    changeError: error
  })),
  on(removeTaskRequest, (state, {taskId}) => ({
    ...state,
    removeLoading: true,
    taskId
  })),
  on(removeTaskSuccess, state => ({
    ...state,
    removeLoading: false,
  })),
  on(removeTaskError, (state, {error}) => ({
    ...state,
    removeLoading: false,
    fetchError: error
  })),
)
