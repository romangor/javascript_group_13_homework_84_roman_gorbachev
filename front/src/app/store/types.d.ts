import { Task } from '../models/task.model';
import { User } from '../models/user.model';

export type TaskState = {
  tasks: Task[],
  fetchLoading: boolean,
  fetchError: null | string,
  createLoading: boolean,
  createError: null | string,
  changeLoading: boolean,
  changeError: null | string,
  removeLoading: boolean,
  removeError: null | string
}

export type AppState = {
  tasks: TaskState
}

export type UserState = {
  users: User[],
  fetchLoading: boolean,
  fetchError: null | string,
}

export type AppUserState = {
  users: UserState
}
