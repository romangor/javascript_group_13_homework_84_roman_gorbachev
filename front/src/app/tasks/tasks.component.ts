import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState, AppUserState } from '../store/types';
import { editTaskRequest, fetchTasksRequest, removeTaskRequest } from '../store/tasks.actions';
import { Task, TaskChange } from '../models/task.model';
import { User } from '../models/user.model';
import { fetchUsersRequest } from '../store/users.actions';


@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.sass']
})
export class TasksComponent implements OnInit {

  tasks: Observable<Task[]>;
  users: Observable<User[]>;
  loading: Observable<boolean>;
  changeLoading: Observable<boolean>;
  removeLoading: Observable<boolean>;
  error: Observable<null | string>;

  constructor(private store: Store<AppState>, private storeUser: Store<AppUserState>) {
    this.tasks = store.select(state => state.tasks.tasks);
    this.loading = store.select(state => state.tasks.fetchLoading);
    this.error = store.select(state => state.tasks.fetchError);
    this.users = storeUser.select(state => state.users.users);
    this.changeLoading = store.select(state => state.tasks.changeLoading);
    this.removeLoading = store.select(state => state.tasks.removeLoading);
  }


  ngOnInit(): void {
    this.store.dispatch(fetchTasksRequest());
    this.storeUser.dispatch(fetchUsersRequest());
  }

  editUser(value: string, id: string, status: string) {
    const taskData: TaskChange= {
      user: value,
      status: status,
      id: id
    }
    this.store.dispatch(editTaskRequest({taskData}));
  }

  async editStatus(value: string, id: string) {
    const taskData: TaskChange= {
      user: '',
      status: value,
      id: id
    }
    await this.store.dispatch(editTaskRequest({taskData}));
    this.storeUser.dispatch(fetchUsersRequest());
  }

  removeTask(taskId: string){
    this.store.dispatch(removeTaskRequest({taskId}));
    this.storeUser.dispatch(fetchUsersRequest());
  }
}
