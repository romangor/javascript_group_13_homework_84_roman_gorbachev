import { Component, OnInit, } from '@angular/core';
import { AppUserState } from '../store/types';
import { Store } from '@ngrx/store';
import { User } from '../models/user.model';
import { Observable } from 'rxjs';
import { fetchUsersRequest } from '../store/users.actions';
import { TaskData } from '../models/task.model';
import { createTaskRequest } from '../store/tasks.actions';

@Component({
  selector: 'app-create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.sass']
})
export class CreateTaskComponent implements OnInit {
  taskText!: string;
  userId!: string;

  users: Observable<User[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;

  constructor(private store: Store<AppUserState>) {
    this.users = store.select(state => state.users.users);
    this.loading = store.select( state => state.users.fetchLoading);
    this.error = store.select( state => state.users.fetchError);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchUsersRequest())
  }

  addTask() {
    const taskData: TaskData = {
      user: this.userId,
      title: this.taskText,
      status: ''
    }
    this.store.dispatch(createTaskRequest({taskData}));
  }
}
