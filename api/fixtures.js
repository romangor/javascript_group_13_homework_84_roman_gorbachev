const mongoose = require('mongoose');
const config = require('./config');
const User = require("./models/User");
const Task = require("./models/Task");
const run = async () => {

    await mongoose.connect(config.mongo.db, config.mongo.options);
    const collections = await mongoose.connection.db.listCollections().toArray()

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [Roman, Lida] = await User.create({
            name: 'Roman'
        },
        {
            name: 'Listen music',
        });

    await Task.create({
            user: Roman,
            title: 'Do homework',
            status: 'new',
        },
        {
            user: Lida,
            title: 'Read book',
            status: 'new',
            image: 'eminem1.jpg'
        },
        {
            user: Roman,
            title: 'Wash a car',
            status: 'new',
        }
    )
    await mongoose.connection.close();
}
run().catch(e => console.error(e))