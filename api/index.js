const express = require('express');
const users = require('./app/users');
const tasks = require('./app/tasks');
const config = require('./config');
const mongoose = require("mongoose");
const cors = require('cors');

const app = express();

const port = 8000;

app.use(express.json());
app.use(cors());
app.use('/users', users);
app.use('/tasks', tasks);
app.use(express.static('public'));

const run = async () => {
    await mongoose.connect(config.mongo.db, config.mongo.options);

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });

    process.on('exit', () => {
        mongoose.disconnect();
    });
}

run().catch(err => console.error(err));