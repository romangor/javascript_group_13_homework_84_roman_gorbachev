const express = require('express');
const Task = require('../models/Task');
const User = require("../models/User");
const router = express.Router();

router.get('/', async (req, res, next) => {
    try {
        const tasks = await Task.find().populate("user", "name");
        return res.send(tasks);
    } catch (e) {
        next(e);
    }
});

router.post('/', async (req, res, next) => {
    try {
        const taskData = {
            user: req.body.user,
            title: req.body.title,
            status: "new"
        };
        const task = new Task(taskData);
        task.save();
        return res.send({message: 'Create new task' + task})
    } catch (e) {
        next(e);
    }
});

router.put('/:id', async (req, res, next) => {
    try {
        const task = await Task.findById(req.params.id);
        if (req.body.user) {
            const user = await User.findById(req.body.user);
            task.user = user
            user.save();
        }
        task.status = req.body.status;
        task.save();
        return res.send({message: 'Task changed ' + task.title});
    } catch (e) {
        next(e);
    }
});

router.delete('/:id', async (req, res, next) => {
    try {
        await Task.deleteOne({_id: req.params.id});
        return res.send({message: 'Task deleted'});
    } catch (e) {
        next(e);
    }
})

module.exports = router;