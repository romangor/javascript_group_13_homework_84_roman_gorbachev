
const rootPath = __dirname;

module.exports = {
    rootPath,
    mongo: {
        db: 'mongodb://localhost/shop',
        options: {useNewUrlParser: true},
    }
};